# Kubernetes manifests YAML collection

This is a collection of useful (or not) YAML files to use to:

* Deploy a `debian-sos` DaemonSet
* Deploy and access to the Kubernetes Dashboard for `1.15`, `1.16` and `1.17` k8s releases
* Create a LoadBalancer in an OVHcloud Managed Kubernetes Service
* Deploy multi-attach persistent volumes with OVHcloud NAS HA
* Managing nodes with the NodePools CRD in an OVHcloud Managed Kubernetes Service
* Formating NVMe disks on IOPS nodes in an OVHcloud Managed Kubernetes Service
* Configure a Keycloak server in an OVHcloud Kubernetes Service
* Deploy several kinds of resources to debug a K8s cluster (pods, deployments, hello-world, LBs, NodePort, ClusterIP, etc.)


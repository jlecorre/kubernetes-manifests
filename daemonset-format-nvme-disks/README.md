# Formating NVME disks on IOPS nodes with OVHcloud MKS

More information about the local NVME disk: https://docs.ovh.com/gb/en/kubernetes/formating-nvme-disk-on-iops-nodes/

## Create a configMap

```bash
kubectl apply -f format-nvme-configmap.yaml
```

## Create a DaemonSet

```bash
kubectl apply -f format-nvme-daemonset.yaml
```

## Add a required label on the targeted nodes

```bash
kubectl label nodes node-nvme-1 node-nvme-2 disktype=nvme
```

## List all `format-nvme` running pods

```bash
kubectl get pod -n kube-system -l 'name=format-nvme' -o wide
```

## Create and use a local NVMe persistant volume

```bash
kubectl apply -f local-nvme-storage-class.yaml
```

## Create a persistent volume

```bash
kubectl apply -f local-nvme-persistent-volume.yaml
```

## Create a persistent volume claim

```bash
kubectl apply -f local-nvme-persistent-volume-claim.yaml
```

## Create and deploy an Nginx pod using the persistent volume clain

```bash
kubectl apply -f local-nvme-nginx-pod.yaml
```

## Check if the local NVME disk is working

```bash
kubectl exec -it local-nvme-nginx -- bash
echo "NVMe disk!" > /usr/share/nginx/html/index.html
exit
kubectl proxy
```

Then open the following URL: http://localhost:8001/api/v1/namespaces/default/pods/http:local-nvme-nginx:/proxy/

## Delete all this stuff (quick and dirty)

```bash
kubectl delete -f .
```

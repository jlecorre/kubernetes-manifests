# How to deploy a LoadBalancer on OVH Managed Kubernetes Service

More information about this LoabBalancer: https://docs.ovh.com/gb/en/kubernetes/using-lb/

```bash
kubectl apply -f create-ovhcloud-loadbalancer-resource.yml
```

# Check if the LoadBalancer has been delivered

## Quick method

```bash
kubectl get svc --watch
```

## Funnier method

```bash
while [[ -z $SERVICE_HOSTNAME ]]; do SERVICE_HOSTNAME=$(kubectl get svc dev-lb -o jsonpath='{.status.loadBalancer.ingress[].hostname}'); echo "Waiting for service external IP..."; sleep 2; done; echo "Your service IP is: http://$SERVICE_HOSTNAME/"
```

# Delete your LoadBalancer

```bash
kubectl delete -f create-ovhcloud-loadbalancer-resource.yml
```

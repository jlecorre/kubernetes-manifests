# Deploy the famous Kubernetes Dashbord

Kubernetes dashboard is a general-purpose web UI for Kubernetes clusters.  
For more information about the Kubernetes Dashboard, please refer to the following repository on Github: [kubernetes / dashboard](https://github.com/kubernetes/dashboard)

# How to deploy it really quickly (no production ready)

1. Go into the folder wich is compatible with your Kubernetes cluster

    ```bash
    cd (1.15|1.16|1.17)
    ```

2. Then deploy the required resources

    ```bash
    kubectl apply -f .
    ```

3. Generate a Bearer Token
    ```bash
    kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user-token | awk '{print $1}') -o json | jq -r '.data.token'
    ```

4. Access the Dashboard using `kubectl proxy`

    ```bash
    kubectl proxy
    ```

5. Open the following URI into your favorite browser and enjoy ;)

    ```bash
    http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/
    ```

# Delete all this stuff

```bash
cd (1.15|1.16|1.17)
kubectl delete -f . 

# or second method
kubectl delete namespace kubernetes-dashboard
kubectl delete -f dashboard-cluster-role-binding.yml
```

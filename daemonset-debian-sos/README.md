# DaemonSet definition to use in "rescue mode"

These `YAML` definition files are useful to deploy a __DaemonSet__ with the following characteristics:

* Use a `ConfigMap` to declare some data to mount into a pod as a `VolumeMount`
* Use a `DaemonSet` to run a pod onto all available nodes in your cluster
* Run an `initContainer` to copy/paste the data embedded in the `ConfigMap` into a specified directory (in this example in the `/tmp/config.test` file)
* Run a Debian container to be able to do some manual checks if required

# How to use the DaemonSet

Execute the following command line to deploy it into your cluster:

* Create the `ConfigMap`:

```bash
kubectl apply -f debian-sos-configmap.yml
```

* Deploy the `DaemonSet`:

```bash
kubectl apply -f debian-sos-daemonset.yml
```

* Verify from the `debian-sos` pod if the config file has been properly created:

```bash
kubectl -n default exec debian-sos -- ls -alh /mnt/config.test
```

* Run a shell in the `debian-sos` pod

```bash
kubectl -n default exec -ti debian-sos -- bash
```

* Verify directly on the first available node of your cluster if the config file has been properly created:

```bash
ssh ${USERNAME}@$(kubectl get nodes -o jsonpath='{.items[0].status.addresses[?(@.type=="InternalIP")].address}') -- ls -alh
```

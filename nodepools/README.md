# How to manipulate Nodepools on OVHcloud Managed Kubernetes Service

More information about the NodePools CRD: https://docs.ovh.com/gb/en/kubernetes/node-pools-crd/

## List node pools

```bash
kubectl get nodepools
kubectl get np
```

## Create a new node pool

```bash
kubectl apply -f create-2-nodes-nodepool.yaml
```

## Editing the node pool size

```bash
kubectl scale --replicas=2 nodepool magical-nodepool
```

## Deleting a node pool

```bash
kubectl delete nodepool magical-nodepool
# or
kubectl delete -f create-2-nodes-nodepool.yaml
```

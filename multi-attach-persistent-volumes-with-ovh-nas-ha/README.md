# How to configure multi-attach persistent volumes with OVH NAS HA

More informations about the OVH NAS HA:

- https://www.ovh.com/world/nas/
- https://docs.ovh.com/gb/en/storage/nas/get-started/
- https://docs.ovh.com/gb/en/kubernetes/configuring-multi-attach-persistent-volumes-with-ovh-nas-ha/

## Let's try to configure and use it

Get our Kubernetes nodes IP:

```bash
kubectl get nodes -o jsonpath='{ $.items[*].status.addresses[?(@.type=="InternalIP")].address }'
```

Configure the OVH NAS HA as described in the documentation (nodes access AND paritionning), then:

```bash
helm install nfs-client-provisioner -n kube-system stable/nfs-client-provisioner -f values.yaml
```

Let's verify our installation:

```bash
helm list -n kube-system
# and
kubectl get deploy nfs-client-provisioner -n kube-system
```

Create the persistent volume claim:

```bash
kubectl apply -f nfs-persistent-volume-claim.yaml
```

Follow the end of the documentation if you want to test the NFS share with some examples pods or enjoy your new NAS HA through your OVHcloud Managed Kubernetes Service.

```bash
kubectl apply -f nfs-nginx-pods.yml
```

## Delete all deployments and pods

```bash
kubectl delete -f nfs-nginx-pods.yaml -f nfs-persistent-volume-claim.yaml
helm uninstall -n kube-system nfs-client-provisioner
```

